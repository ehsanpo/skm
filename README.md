# README #

## Install
Install with `npm install`

## CLI 
Run `node index.js`

example usage: 
`node index.js 4 4 2 2 1 1 0`

## APi version 
Run `node_modules/.bin/babel-node Api.js`

Send a req to http://localhost:5000/api/v1/send

exsample usage:
`
curl -X POST \
  http://localhost:5000/api/v1/send \
  -H 'Content-Type: application/javascript' \
  -H 'cache-control: no-cache' \
  -d '{
	"command" : [4,4,2,2,1]
}
'
`