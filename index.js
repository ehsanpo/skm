//Start command line

//get arg

//import Object_Class from "";

const Object_Class = require("./Object.js");

const argv = require("yargs").argv;

let grid = { x: argv._[0], y: argv._[1] };
let commands = argv._.splice(4);

//create Object
let object = new Object_Class(argv._[2], argv._[3], grid);

//Loop commands
for (var i = 0; i < commands.length; i++) {
	if (object.is_done) break;

	if (!object.move(commands[i])) {
		object.x = -1;
		object.y = -1;
		break;
	}
}

//output
if (typeof argv.json !== "undefined") {
	// --Json
	console.log(object.json());
} else {
	object.CL();
}

process.exit();
