//object class
class Object_Class {
	constructor(x, y, grid) {
		this.x = x;
		this.y = y;
		this.grid = grid;
		this.direction = "N";
		this.done = false;
		this.info = () => `The X is ${this.x} & Y ${this.y}`;
		this.CL = () => console.log(`[${this.x},${this.y}]`);
		this.json = () => ({ x: this.x, y: this.y });
	}

	change_direction(moveto) {
		switch (this.direction) {
			case "N":
				if (moveto == 3) this.direction = "E";
				if (moveto == 4) this.direction = "W";
				break;

			case "S":
				if (moveto == 3) this.direction = "W";
				if (moveto == 4) this.direction = "E";

				break;
			case "E":
				if (moveto == 3) this.direction = "S";
				if (moveto == 4) this.direction = "N";

				break;
			case "W":
				if (moveto == 3) this.direction = "N";
				if (moveto == 4) this.direction = "S";

				break;
			default:
				throw "Error From change_direction()";
		}
	}
	move(moves) {
		switch (moves) {
			case 0:
				this.is_done = true;
				break;
			case 1:
				if (this.direction == "N") {
					this.y--;
				}
				if (this.direction == "S") {
					this.y++;
				}
				if (this.direction == "E") {
					this.x++;
				}
				if (this.direction == "W") {
					this.x--;
				}
				break;
			case 2:
				if (this.direction == "N") {
					this.y++;
				}
				if (this.direction == "S") {
					this.y--;
				}
				if (this.direction == "E") {
					this.x--;
				}
				if (this.direction == "W") {
					this.x++;
				}
				break;

			case 3:
				this.change_direction(3);
				break;
			case 4:
				this.change_direction(4);
				break;

			default:
				throw "Error From move() , move command not find ";
		}

		return this.x < 0 ||
			this.y < 0 ||
			this.x >= this.grid.x ||
			this.y >= this.grid.y
			? false
			: true;
	}
}
module.exports = Object_Class;
