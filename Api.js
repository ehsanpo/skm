var express = require("express");
var app = express();

app.post("/api/v1/send", express.json({ type: "*/*" }), (req, res) => {
	console.log(req.body);
	const result = api_connect(req.body);
	res.status(200).send({
		success: "true",
		result: result,
	});
});

app.listen(5000);

function api_connect(req_body) {
	let argv = req_body.command;

	const Object_Class = require("./Object.js");
	let grid = { x: argv[0], y: argv[1] };
	let commands = argv.splice(4);

	//create Object
	let object = new Object_Class(argv[2], argv[3], grid);

	for (var i = 0; i < commands.length; i++) {
		if (object.is_done) break;

		if (!object.move(commands[i])) {
			object.x = -1;
			object.y = -1;
			break;
		}
	}

	return object.json();
}
